This plugin should:
1. enqueue Bootstrap css and js for their modal stuff
2. enqueue a custom show/hide script
3. enqueue a custom css
4. have controls for:
  1. enable modal
  2. link url (where the modal link will point to)
  3. select image
  4. show current time
  5. field to change expiration time
