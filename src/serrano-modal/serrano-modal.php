<?php
/*
Plugin Name: Serrano Modal
Plugin URI: https://steveclason@bitbucket.org/steveclason/serranopopup.git
Description: Home page modal window.
Version: 0.1
Author: Steve Clason
Author URI: http:www.steveclason.com
License: GPLv3
*/

/*
Copyright 2015 Steve Clason, (email: steve@steveclason.com).

This file is part of Serrano Modal.

Serrano Modal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Serrano Modal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

For a copy of the GNU General Public License see <http://www.gnu.org/licenses/>.
*/

register_activation_hook( __FILE__, 'serrano_modal_install' );

function serrano_modal_install() {
  global $wp_version;
  if( version_compare( $wp_version, '4.1', '<' ) ) {
    wp_die( 'This plugin requires WordPress version 4.2 or higher.' );
  }
}

register_deactivation_hook( __FILE__, 'serrano_modal_deactivate' );

function serrano_modal_deactivate() {

}

/* Load modal scripts and styles for page display. */
function serrano_modal_scripts_styles() {
  wp_register_script ( 'modaljs' , plugins_url( '/js/bootstrap.min.js',  __FILE__), array( 'jquery' ), '', true );
  wp_register_script( 'serrano-modal', plugins_url( '/js/serrano-modal.js', __FILE__ ), array( 'jquery' ), '', true );
	wp_register_style ( 'modalcss' , plugins_url( '/css/bootstrap.css',  __FILE__), array() , '', 'all' );

	wp_enqueue_script( 'modaljs' );
  wp_enqueue_script( 'serrano-modal' );
	wp_enqueue_style( 'modalcss' );
}
add_action( 'wp_enqueue_scripts', 'serrano_modal_scripts_styles' );

/* Load admin styles. */
function serrano_modal_admin_styles() {
  wp_enqueue_style('thickbox');
}
add_action('admin_print_styles', 'serrano_modal_admin_styles');

/* Load admin scripts. */
function serrano_modal_admin_scripts() {
  wp_enqueue_script('jquery');
  wp_enqueue_media();
}
add_action('admin_print_scripts', 'serrano_modal_admin_scripts');



function serrano_create_modal_submenu() {
  add_options_page(
    'Serrano Modal Settings Page',
    'Serrano Modal Settings',
    'manage_options',
    'serrano_modal_settings_menu',
    'serrano_modal_settings_page'
  );
  // Register-settings function call went here.
  add_action( 'admin_init', 'serrano_modal_register_settings' );
}
add_action( 'admin_menu', 'serrano_create_modal_submenu' );

// Store all the settings in a single array.
function serrano_modal_register_settings() {
  register_setting(
    'serrano_modal_settings_group',
    'serrano_modal_options',
    'serrano_modal_sanitize_options'
  );
}


/*
Options:
  serrano_modal_enable -- checkbox
  serrano_modal_img -- media selector
  serrano_modal_uri -- text or article selector
  serrano_modal_expires -- date/time entry
  [disabled] serrano_modal_current_time -- server timestamp, WP current_time()
*/
// The form for selection configuration options.
function serrano_modal_settings_page() { ?>
  <div class="wrap">
    <script language="JavaScript" type="text/javascript">
      jQuery( document ).ready( function( $ ){
          $('#select_image_button').click(function( e ) {
              e.preventDefault();
              var image = wp.media({
                  title: 'Upload Image',
                  // mutiple: true if you want to upload multiple files at once
                  multiple: false
              }).open()
              .on( 'select', function( e ){

                  // This will return the selected image from the Media Uploader, the result is an object
                  var uploaded_image = image.state().get( 'selection' ).first();

                  // We convert uploaded_image to a JSON object to make accessing it easier
                  var image_url = uploaded_image.toJSON().url;

                  // Assign the url value to the input field.
                  // Escape the brackets else they're attributes.
                  $( '#serrano_modal_options\\[serrano_modal_img\\]' ).val( image_url );
                  $( '#modal_img_thumb' ).attr( 'src', image_url );
              });
          });
      });
    </script>


    <h2>Serrano Modal Window Configuration Options</h2>
    <p>
      Use shortcode "[serrano_modal]" inside body tag of header.
    </p>
    <form method="post" action="options.php">
      <?php settings_fields( 'serrano_modal_settings_group' ); ?>
      <?php $serrano_modal_options = get_option( 'serrano_modal_options' ); ?>

      <p>
        <label for="serrano_modal_options[serrano_modal_enable]">Enable Modal Window</label>
        <input type="checkbox" name="serrano_modal_options[serrano_modal_enable]" id="serrano_modal_options[serrano_modal_enable]"
          <?php checked( $serrano_modal_options['serrano_modal_enable'], 'on', true ) ?> />
      </p>
      <p>
        <label for="serrano_modal_options[serrano_modal_uri]">Link to:</label>
        <br/>
        <input type="text" size="36"
          id="serrano_modal_options[serrano_modal_uri]"
          name="serrano_modal_options[serrano_modal_uri]"
          value="<?php echo $serrano_modal_options['serrano_modal_uri']; ?>"
          placeholder= "htp://www.some_url/path.com" />
      </p>
      <p>
        <label for="serrano_modal_options[serrano_modal_img]">Modal Window Image</label>
        <br/>
  		  <input type="text" size="36"
          id="serrano_modal_options[serrano_modal_img]"
          name="serrano_modal_options[serrano_modal_img]"
          value="<?php echo $serrano_modal_options['serrano_modal_img']; ?>" />
        <input id="select_image_button" type="button" value="Select Image" />
        <div class="modal-image">

          <?php if( $serrano_modal_options['serrano_modal_img'] ) { ?>
          <img id="modal_img_thumb" src="<?php echo $serrano_modal_options['serrano_modal_img']; ?>" />
          <?php } else { ?>
            No image selected.
          <?php } ?>
        </div>
        <input type="hidden" name="serrano_modal_options[serrano_modal_current_time]" id="serrano_modal_options[serrano_modal_current_time]"
            value="<?php echo current_time( 'm/d/Y H:i:s' ); ?>" />
            Current time is: <?php echo current_time( 'm/d/Y H:i:s' ); ?>
      </p>
      <p>
        <label for="serrano_modal_options[serrano_modal_expires]">Expires at: </label>
        <input name="serrano_modal_options[serrano_modal_expires]" id="serrano_modal_options[serrano_modal_expires]"
            value="<?php echo esc_attr( $serrano_modal_options['serrano_modal_expires'] ); ?>" />
      </p>
      <?php
      // Give a little warning if the offer has expired.
      if( current_time( 'm/d/Y H:i:s' ) > $serrano_modal_options['serrano_modal_expires'] ) {
        echo "<p class='warning'>This offer has expired and is no longer visible.</p>";
      }
      ?>
      <p>
        <input type="submit" class="button-primary" value="Save Changes" />
      </p>
    </form>
  </div>
<?php
}

// TODO Finish this.
/* Clean up the form inputs using WordPress utility function. */
function serrano_modal_sanitize_options( $input ) {
  $input['serrano_modal_enable'] = ( $input['serrano_modal_enable'] == 'on' ) ? 'on' : '';
  $input['serrano_modal_img'] = sanitize_text_field( $input['serrano_modal_img'] );
  $input['serrano_modal_current_time'] = $input['serrano_modal_current_time'];
  $input['serrano_modal_expires'] = $input['serrano_modal_expires'];

  return $input;
}

// Shortcode looks like "[serrano_modal]".
function serrano_modal_shortcode( $atts ) {

  $modal_options = get_option( 'serrano_modal_options' );

  if( is_front_page() && $modal_options['serrano_modal_enable'] = 'on' ) { ?>

	<div class="modal" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
					<script type="text/javascript">
						var serranoModal = {};
						serranoModal.expires = "<?php echo $modal_options['serrano_modal_expires']; ?>";
						serranoModal.currentTime = "<?php echo $modal_options['serrano_modal_current_time']; ?>";
					</script>
					<div class='counter-wrapper'>This offer expires
						<span id='counter'></span>
					</div>

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#xf05c;</i></button>
        </div>
        <div class="modal-body">
              <a href="<?php echo $modal_options['serrano_modal_uri']; ?>">
								<img src="<?php echo $modal_options['serrano_modal_img']; ?>" />
							</a>
        </div>
    	</div>
  	</div>
	</div>

  <?php
  }
}

add_shortcode( 'serrano_modal', 'serrano_modal_shortcode' );
