	/* Modal Pop Up */

	/* Countdown Timer */
	//CountDownTimer('02/19/2012 10:1 AM', 'countdown');
  //CountDownTimer('02/20/2012 10:1 AM', 'newcountdown');
jQuery(document).ready(function($){

  function countDownTimer(expires, serverNow, element ) {
    var end = new Date( expires );
    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;

    function showRemaining() {
      var now = new Date( serverNow );
      var distance = end - now;
      if (distance < 0) {

          clearInterval(timer);
          document.getElementById( element ).innerHTML = 'EXPIRED!';

          return;
      }
      var days = Math.floor(distance / _day);
      var hours = Math.floor((distance % _day) / _hour);
      var minutes = Math.floor((distance % _hour) / _minute);
      var seconds = Math.floor((distance % _minute) / _second);

			// Manage plurals.
      // "This offer expires..." so "...today" or ..."in X day(s)".
			var msg = '';
			if( days !== 0 ){
				msg += 'in ' + days + ' day';
				if( days !== 1 ) {
					msg += 's.';
				} else {
					msg += '.';
				}
			} else {
        msg += 'today.';
      }

			/*if( hours !== 0 ){
				msg += ', ' + hours + ' hour';
				if( hours !== 1 ) {
					msg += 's ';
				} else {
					msg += ' ';
				}
			}

			if( minutes !== 0 ){
				msg += ', ' + minutes + ' minute';
				if( minutes !== 1 ) {
					msg += 's ';
				} else {
					msg += ' ';
				}
			}

			if( seconds !== 0 ){
				msg += ', and ' + seconds + ' second';
				if( seconds !== 1 ) {
					msg += 's.';
				} else {
					msg += '.';
				}
			} */

			document.getElementById( element ).innerHTML = msg;
  }

	showRemaining();
}

	/* Exposes the modal. */
	function showModal() {
		var expires = serranoModal.expires;
		var serverNow = serranoModal.currentTime;
		//var serverNow = 'Wed Aug 09 2015 17:31:45 GMT-0600 (Mountain Standard Time)';
		var	element = 'counter';

		//Abort if expiration is passed.
		if( expires < serverNow ) {
			return;
		}

		$( '#basicModal' ).show();
		$( '.modal-content' ).animate({
			opacity: 1
		}, 2000 );

		if( serranoModal.expires !== '' ) {
			countDownTimer( expires, serverNow, element );
		}

		// Set a cookie so the poor sap doesn't have to see the modal again.
		document.cookie = 'beenThere=yes';
	}

	// If the modal exists, fade it in.
	if( $( '#basicModal' ).length !== 0 ) {

		// Check for cookie and show if absent.
		var beenThere = document.cookie.replace(/(?:(?:^|.*;\s*)beenThere\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		if( beenThere !== 'yes' ) {
			window.setTimeout( showModal, 1000 );
		}
	}

	// Close the modal on button click.
	$( 'button.close' ).bind( 'click', function() {
		$( '#basicModal' ).hide();
	});

});
