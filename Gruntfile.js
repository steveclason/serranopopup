
module.exports = function(grunt){

  grunt.loadNpmTasks( "grunt-contrib-compass" );
  grunt.loadNpmTasks( "grunt-contrib-watch" );
  grunt.loadNpmTasks( "grunt-copy-to" );
  grunt.loadNpmTasks( "grunt-ftpush" );

  grunt.initConfig({
    compass: {
      dev: {
        options: {
          sassDir: "src/css/sass",
          cssDir: "dist/css",
          sourcemap: true,
          outputStyle: "nested"
        }
      }
    },
    // Copy necessary src files to dist.
    copyto: {
      main: {
        files: [
          {
            expand: true,
            cwd: 'src/',
            src: ['**/*'],
            dest: 'dist'
          }
        ]
      },
    },
    watch: {
      sass: {
        files: "src/css/sass/**/*",
        tasks: ["compass", "copyto", "ftpush:dev"]
      },
      code: {
        files: ["src/*",
          "src/_inc/**/*",
          "src/js/**/*",
          "scr/**"
          ],
        tasks: ["copyto", "ftpush:dev"]
      }
    },
    ftpush: {
      dev: {
        auth: {
          host: 'dev.serranohotel.com',
          port: '21',
          authKey: 'auth-key'
        },
        src: 'dist',
        dest: '/dev.serranohotel.com/html/wp-content/plugins',
        exclusions: [],
        keep: [],
        simple: true,
        useList: false
      }
    }
  });

  grunt.registerTask( 'deploy', ['compass', 'copyto', 'ftpush:dev']);
  grunt.registerTask( 'default', ['compass', 'copyto']);
  grunt.registerTask( 'push', ['copyto', 'ftpush']);
};
